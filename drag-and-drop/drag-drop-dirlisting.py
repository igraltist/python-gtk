#! /usr/bin/env python27
#
# Example: gtk3 drag and drop with dirlisting 
#
# Copyright 2017 Jens Kasten <jens.kasten@kasten-edv.de> 
# 

import os
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk

GDK_2BUTTON_PRESS = getattr(Gdk.EventType, "2BUTTON_PRESS")

GLADE_WINDOW = "drag-drop-dirlisting.glade"
REMOTE = ["remote1", "remote2", "remote3"]

builder = Gtk.Builder()
builder.add_from_file(GLADE_WINDOW)


class Tree(object):
    
    def __init__(self, title):
        self.liststore = None
        self.treeview = None
        self.title = title
        self.cell = Gtk.CellRendererText()
        self.column = Gtk.TreeViewColumn(self.title, self.cell, text=0)
        # FIX: 
        self.path_separator = "/" 

    def get_value(self):
        """return the selected value from treeview liststore"""
        selection = self.treeview.get_selection()
        model, path = selection.get_selected_rows()
        tree_iter = model.get_iter(path)
        value = model.get_value(tree_iter, 0)
        return value

    def add_list_element(self, element):
        self.liststore.append([element])

    def clear_list(self):
        for row in self.liststore:
            self.liststore.remove(row.iter)
        self.liststore.clear()

    def append_column(self):
        self.treeview.append_column(self.column)

    def set_liststore(self, liststore):
        self.liststore = liststore
    
    def set_treeview(self, treeview, widget=None):
        """enable for LocalTree the drag source and destination"""
        # FIX: set target insteed emply list and add widget to TargetFlags
        self.treeview = treeview
        self.treeview.drag_source_set(Gdk.ModifierType.BUTTON1_MASK, [],
            Gdk.DragAction.COPY)
        self.treeview.drag_source_add_text_targets()

        self.treeview.drag_dest_set(Gtk.DestDefaults.ALL, [],
            Gdk.DragAction.COPY)
        self.treeview.drag_dest_add_text_targets()


class LocalTree(Tree):

    def __init__(self, title):
        Tree.__init__(self, title)
        self.current_dir = os.environ.get("HOME", 
            os.path.dirname(os.path.abspath(__file__)))

    def add_current_dir_to_list(self):
        self.add_list_element("..")
        try:
            files = os.listdir(self.current_dir)
            files.sort()
            for i in files:
                self.add_list_element(i)
        except IOError as error:
            return False

    def set_home_dir(self):
        self.add_current_dir_to_list()

    def data_received(self, widget, context, x, y, data, info, time):
        text = data.get_text()
        self.add_list_element(text)

    def data_get(self, widget, context, data, info, time):
        """Called on window to when drag starting"""
        value = self.get_value()
        # only allow drag when is a file
        if os.path.isfile(os.path.join(self.current_dir, value)):
            data.set_text(self.get_value(), -1)
            context.finish(True, False, time)

    def remove_last_pathelement(self):
        temp = self.current_dir.split(self.path_separator)
        temp.pop()
        if len(temp) == 1:
            # for windows must set like C:
            temp = "/"
            return temp
        else:
            return self.path_separator.join(temp)

    def change_dir(self, sender, event):
        # doubleclick for change dir only
        if GDK_2BUTTON_PRESS == event.type:
            value = self.get_value()
            if value == "..":
                self.current_dir = self.remove_last_pathelement()
            elif os.path.isdir(os.path.join(self.current_dir, value)):
                self.current_dir = os.path.join(self.current_dir, value)
            self.clear_list()
            self.add_current_dir_to_list()


class RemoteTree(Tree):
    
    def add_list(self):
        for i in REMOTE:
            self.add_list_element(i)
    
    def data_get(self, widget, context, data, info, time):
        value = self.get_value()
        # FIX: check for remote site the request is a file
        if True:
            data.set_text(self.get_value(), -1)
            context.finish(True, False, time)

    def data_received(self, widget, context, x, y, data, info, time):
        # FIX: check if connected
        text = data.get_text()
        self.add_list_element(text)


class Connection(object):
    
    def __init__(self):
        self.status = None
        self.statusbar = None
        self.remotetree = None

    def set_statusbar(self, statusbar):
        self.statusbar = statusbar
    
    def set_remotetree(self, remotetree):
        self.remotetree = remotetree

    def open(self, source):
        if self.status:
            return True
        self.status = True
        self.statusbar.push(1, "connected")
        self.remotetree.add_list()
   
    def close(self, source):
        if not self.status:
            return True 
        
        self.status = False
        self.statusbar.push(1, "disconnected")
        self.remotetree.clear_list()
        

def main():
    treeview_local = builder.get_object("treeview_local")
    liststore_local = builder.get_object("liststore_local")
    treeview_remote = builder.get_object("treeview_remote")
    liststore_remote = builder.get_object("liststore_remote")
    statusbar = builder.get_object("Statusbar")

    title = "Local"
    localtree = LocalTree(title)
    localtree.set_liststore(liststore_local)
    localtree.set_treeview(treeview_local)
    localtree.append_column()

    title = "Remote"
    remotetree = RemoteTree(title)
    remotetree.set_liststore(liststore_remote)
    remotetree.set_treeview(treeview_remote)
    remotetree.append_column()

    connection = Connection()
    connection.set_remotetree(remotetree)
    connection.set_statusbar(statusbar)

    handlers = {
        "onDeleteWindow": Gtk.main_quit,
        "onQuit": Gtk.main_quit,
        "onAbout": Gtk.AboutDialog,
        "onConnect": connection.open,
        "onDisconnect": connection.close,
        "local_data_get": localtree.data_get,
        "local_data_received": localtree.data_received,
        "local_change_dir": localtree.change_dir,
        "remote_data_received": remotetree.data_received,
        "remote_data_get": remotetree.data_get,
    }
    builder.connect_signals(handlers)

    statusbar.push(1, "disconnected")

    localtree.set_home_dir()

    window = builder.get_object("DragAndDrop")
    window.show_all()
    Gtk.main()


if __name__ == "__main__":
    main()
